package project.riteh.keyboardnks;

import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputConnection;

public class Keyboard extends InputMethodService implements KeyboardView.OnKeyboardActionListener {

    private KeyboardView kv;
    private android.inputmethodservice.Keyboard keyboard;
    private boolean caps = false;
    char [] dotPeriod = null;
    int counter = 0;
    long time = -1;
    InputConnection ic = getCurrentInputConnection();
    CountDownTimer countTime = new CountDownTimer(3000, 100) {
        public void onTick(long millisUntilFinished) {
            time = millisUntilFinished;
        }
        public void onFinish() {
            getCode();
            time = -1;
            counter = 0;
            dotPeriod = null;
        }
    };

    @Override
    public View onCreateInputView() {
        kv = (KeyboardView)getLayoutInflater().inflate(R.layout.keyboard, null);
        keyboard = new android.inputmethodservice.Keyboard(this, R.xml.keys);
        kv.setKeyboard(keyboard);
        kv.setOnKeyboardActionListener(this);
        return kv;
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        playClick(primaryCode);

        countTime.cancel();
        makeCode();
        countTime.start();


        switch(primaryCode){
            case android.inputmethodservice.Keyboard.KEYCODE_DELETE :
                ic.deleteSurroundingText(1, 0);
                break;
            case android.inputmethodservice.Keyboard.KEYCODE_SHIFT:
                caps = !caps;
                keyboard.setShifted(caps);
                kv.invalidateAllKeys();
                break;
            case android.inputmethodservice.Keyboard.KEYCODE_DONE:
                ic.sendKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_ENTER));
                break;
            default:
                char code = (char)primaryCode;
                if(Character.isLetter(code) && caps){
                    code = Character.toUpperCase(code);
                }
                ic.commitText(String.valueOf(code),1);
        }
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }

    private void playClick(int keyCode){
        AudioManager am = (AudioManager)getSystemService(AUDIO_SERVICE);
        switch(keyCode){
            case 32:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_SPACEBAR);
                break;
            case android.inputmethodservice.Keyboard.KEYCODE_DONE:
            case 10:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_RETURN);
                break;
            case android.inputmethodservice.Keyboard.KEYCODE_DELETE:
                am.playSoundEffect(AudioManager.FX_KEYPRESS_DELETE);
                break;
            default: am.playSoundEffect(AudioManager.FX_KEYPRESS_STANDARD);
        }
    }

    private void makeCode(){
        if (time <=500 && time != -1){
            dotPeriod[counter] = '.';
            counter++;
        }else if(time <=1200){
            dotPeriod[counter] = '_';
            counter++;
        }
    }

    private void getCode(){
        String tempCode = String.valueOf(dotPeriod);
        if (tempCode.equals(".")){
            int code = 101;
            ic.commitText(String.valueOf((char)code),1);
        }else if (tempCode.equals("..")){
            int code = 105;
            ic.commitText(String.valueOf((char)code),1);
        }else if (tempCode.equals("._")){
            int code = 97;
            ic.commitText(String.valueOf((char)code),1);
        }

    }
}
